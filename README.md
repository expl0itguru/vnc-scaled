# Purpose
`vncdesk` is a Linux application which allows for VNC sessions to be created in a similar style to switching to a new TTY and running `startx`. However, a couple extras creature comforts have been added for things like scaling and taking care of all the underlying hackery. The aim of this configuration is to create a scaled environment for non-DPI-aware applications, such as Critix and other GTK2.x based apps.

# Getting setup
Grab `vncdesk` from your distribution's package manager, e.g. `yay -S vncdesk-git` for Arch, or directly from GitHub: https://github.com/feklee/vncdesk.

To get a structure for vncdesk setup in the format it expects:
```shell
# Create the directory if it does not exist yet
$ cd ~/.vncdesk
$ git clone https://gitlab.com/expl0itguru/vnc-scaled.git

# Move the config to the working directory
$ mv vncdesk-scaled/1 .

# Move the script into an executable directory within your $PATH
$ sudo mv vncdesk-scaled/vscaled /usr/bin

# Clean up
$ rm -rf vncdesk-scaled
```

# Fixing sizing and positioning
Open `~/.vncdesk/1/settings.ini` and edit properities as desired. The default configuration is designed for 4K UHD monitors with spacing left for the window manager and dock.

# Testing it out
Try executing `vscaled <app name>`. For example, `vscaled xfce4-terminal`. Note that when `vscaled` launches `vncdesk`, it adds `GDK_SCALE=1` to the environment to allow all scaling to be taken care by `vncdesk`. This typically looks better than allowing the window manager to take care of it. You may want to remove this if you find the window frame of the VNC view is not the right size.
